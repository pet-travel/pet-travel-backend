const router = require("express").Router();
const Bookmark = require("../model/bookmark")
const jwt = require("jsonwebtoken");

router.get('/', async (req, res) => {
    const result = jwt.verify(req.headers.authorization.split(" ")[1],"secret")
    const bookmarks = await Bookmark.find({userId:result._id}).populate({path:"userId",select:"-password -role"}).populate("placeId")
    res.send(bookmarks)
})

router.post('/', async (req, res) => {
    const result = jwt.verify(req.headers.authorization.split(" ")[1],"secret")
    await Bookmark.create({userId:result._id,placeId:req.body.placeId})
    res.send("ADD BOOKMARK COMPLETED")
})

router.delete('/:id', async(req, res)=>{
    await Bookmark.deleteOne({_id:req.params.id})
    res.send("DELETE BOOKMARK COMPLETED")
})

module.exports = router