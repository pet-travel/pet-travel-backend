const router = require("express").Router();
const User = require("../model/user")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken");
const user = require("../model/user");

router.get("/", async(req, res) => {
    const users = await User.find()
    res.send(users)
})

router.post("/register",async(req, res) => {
    const user = await User.findOne({email: req.body.email})
    if(user){
        res.send("REGISTER FAILED")
    }
    else{
        req.body.password = await bcrypt.hash(req.body.password,10)
        await User.create(req.body)
        res.send("REGISTER COMPLETED")
    } 
})

router.post("/login",async(req, res) =>{
    const user = await User.findOne({ email: req.body.email})
    if (!user){
        res.send("LOGIN FAILED")
        return
    }
    
    if (!bcrypt.compareSync(req.body.password,user.password)){
        res.send("LOGIN FAILED")
        return
    }
    const token = jwt.sign({_id:user._id},"secret")

    res.send({token,user})
})

router.get('/profile', async(req, res)=>{  
    const result = jwt.verify(req.headers.authorization.split(" ")[1],"secret")    
    const user = await User.findOne({_id:result._id}).select("-password -role")
    res.send(user)
})

router.patch('/',  async(req, res)=>{
    const result = jwt.verify(req.headers.authorization.split(" ")[1],"secret")
    await User.updateOne({_id:result._id},{$set:req.body})
    res.send("EDIT PROFILE COMPLETED")
})

router.delete('/:id', async(req, res)=>{
    await User.deleteOne({_id:req.params.id},)
    res.send("DELETE COMPLETED")
})


module.exports = router

