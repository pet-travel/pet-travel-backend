const mongoose= require('mongoose')
module.exports = mongoose.model("user",new mongoose.Schema({
    name: String,
    nickname: String,
    phonenumber: String,
    email:String,
    password:String,
    role:String
}))