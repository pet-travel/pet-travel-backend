const mongoose= require('mongoose')
module.exports = mongoose.model("place",new mongoose.Schema({
    name: String,
    locationId: {type:mongoose.Types.ObjectId,ref:"locations"},
    location : String,
    district: String,
    detail: String,
    img: String,
    imgs: [String]
})) 